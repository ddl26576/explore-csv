package com.delawarelife.example.file;

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "myconfig")
data class MyConfig(
    val id: String,
    val user: String,
    val secret: String
)