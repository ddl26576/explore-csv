package com.delawarelife.example.file.record

import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.io.ByteArrayOutputStream
import java.io.OutputStream
import kotlin.reflect.KClass

class CSVRecordStreamBuilder : RecordStreamBuilder() {
    private var mapper: CsvMapper = CsvMapper()
    private var csvSchema: CsvSchema? = null
    private var specRecord: KClass<out Any>? = null
    private var useHeader: Boolean = false

    override fun spec(detailRecord: KClass<out Any>): CSVRecordStreamBuilder {
        specRecord = detailRecord
        csvSchema = mapper setupSchemaWith detailRecord
        return this
    }

    private infix fun CsvMapper.setupSchemaWith(specRecord: KClass<out Any>): CsvSchema {
        return this.schemaFor(specRecord.java)
            .withoutHeader()
//            .withoutQuoteChar()
            .withQuoteChar('"')
            .withColumnSeparator(',')
            .withLineSeparator("\n")
            .withAllowComments(true)
    }

    override fun header(useHeader: Boolean): CSVRecordStreamBuilder {
        this.useHeader = useHeader
        return this
    }

    override fun build(outputStream: OutputStream): RecordStream {
        if (csvSchema==null) {
            throw IllegalArgumentException("must define spec before calling build")
        }
        mapper.registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
        if (useHeader)  {
            val headerSchema = (mapper setupSchemaWith specRecord !! ).withUseHeader(true)
            val headerString = mapper.writer(headerSchema).writeValueAsString(null)
            outputStream.write(headerString.toByteArray())
        }
        return CSVRecordStream(mapper.writer(csvSchema), outputStream)
    }

}