package com.delawarelife.example.file.record

import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.dataformat.csv.CsvSchema
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.io.OutputStream
import kotlin.reflect.KClass

abstract class RecordStreamBuilder {
    abstract fun spec(detailRecord: KClass<out Any>): RecordStreamBuilder
    abstract fun header(useHeader: Boolean = true): RecordStreamBuilder
    abstract fun build(outputStream: OutputStream): RecordStream
}