package com.delawarelife.example.file.record

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonRawValue
import java.math.BigDecimal
import java.time.LocalDate

/**
 * Common reused values
 */
const val DATE_FORMAT = "yyyy/MM/dd"

/**
 *

#Ledger Detail Record

Position    |Required	| Name        					                |Type    	|Size/Format  			    |Example    		        |Comment
------------|:---------:|:----------------------------------------------|-----------|--------------------------:|:-------------------------:|:---------------------------------------------------------------
1	  	    |Y		    |Status Code	 				                |Varchar	|3	        			    |NEW			            |Status should be "NEW"
2	  	    |Y		    |Ledger ID	 				                    |Number		|18		        		    |300000001542008	        |Ledger ID shall be 300000001542008 for all files incoming to DL Common Primary Ledger
3	  	    |Y		    |Effective Date of Transaction	                |Date		|YYYY/MM/DD	        	    |2019/05/04		            |Accounting Date of the journal (YYYY/MM/DD) = process date in Onyx
4	  	    |Y		    |(L)Journal Source				                |Varchar	|25				            |Andesa			            |Each feeder will has its own code. For ONYX, the source will be “Onyx”
5	  	    |Y		    |(L)Journal Category			                |Varchar	|25				            |Feeder			            |All regular feeds will have "Feeder" for this field
6	  	    |Y		    |(L)Currency Code				                |Varchar	|3 				            |USD			            |Transaction Currency- for Onyx, it should all be USD
7	  	    |Y		    |Journal Entry Creation Date	                |Date		|YYYY/MM/DD			        |2019/04/07		            |Journal Creation Date (YYYY/MM/DD)- will equal accounting date
8	  	    |Y		    |Actual Flag					                |Varchar	|1				            |A			                |Actual Flag should be A for all regular feeders
9	  	    |Y		    |(L)Legal Entity				                |Varchar	|3				            |G01			            |Must contain a valid value (Cannot be zero filled).  For Masters Prime, this will either be G01 or S01. The legal entity code is determined by the accounting. See “accounting” document. Separate Account = S01; General Account =G01
10	  	    |Y		    |(L)Major Product				                |Varchar	|4				            |1000			            |Must contain a valid value (Cannot be zero filled) For Masters Prime, when feeding G01 in position #9, this field will equal “3002” when the policy is in deferral; “7000” when the policy is in payout and has life contingencies and “7001” when the policy is PERIOD certain only (Exhibit 7 reserve).  When feeding S01 in field #9, this field will be “3113”
11	  	    |Y		    |(L)Account					                    |Varchar	|6				            |013120			            |Must contain a valid value (Cannot be zero filled) Please refer to the DLH Account number in the accounting document. This will change depending of the transaction processed
12	  	    |Y		    |(L)Cost Center					                |Varchar	|5				            |31001			            |For Masters Prime, no Cost Center so use 00000
13	  	    |Y		    |(L)Residence					                |Varchar	|2				            |AK			                |All transaction coming from Onyx Admin system needs to populate the State of the policyholder residence. Listing of residence codes provided separately. For trading system and commission system, feed “00”
14	  	    |Y		    |(L)Reinsurance					                |Varchar	|2				            |01			                |For Onyx, this needs to be 00
15  	  	|Y		    |(L)Inter-Company				                |Varchar	|3				            |000			            |For Onyx, this needs to be 000
16	  	    |Y		    |(L)Business Type				                |Varchar	|1				            |0			                |For Onyx, this needs to be 2
17	  	    |Y		    |(L)Market Segment				                |Varchar	|1				            |0			                |For transactions coming from the admin system, we need Onyx to feed Q or N. Q will be needed for Qual and N for Non Qual. Any other cases, feed 0
18	  	    |Y		    |(L)Fund					                    |Varchar	|3				            |000		    	        |If the policyholder invests in fixed funds, feed 000. If policyholder invests in variable account, feed a 3 digit code as provided in a separate document. If new funds are added, a new code will need to be built in the system and ledger.
19	  	    |		    |Segment11	 	 			                    |Varchar	|100				        |			                |Not  Required
20	  	    |		    |Segment12	 	 			                    |Varchar	|100				        |			                |Not  Required
21	  	    |		    |Segment13	 	 			                    |Varchar 	|100				        |			                |Not  Required
22	  	    |		    |Segment14	 	 			                    |Varchar 	|100				        |			                |Not  Required
23	  	    |		    |Segment15	 	 			                    |Varchar 	|100				        |			                |Not  Required
24	  	    |		    |Segment16	 	 			                    |Varchar 	|100				        |			                |Not  Required
25	  	    |		    |Segment17	 	 			                    |Varchar	|100				        |			                |Not  Required
26	  	    |		    |Segment18	 	 			                    |Varchar 	|100				        |			                |Not  Required
27	  	    |		    |Segment19	 	 			                    |Varchar	|100				        |			                |Not  Required
28	  	    |		    |Segment20	 	 			                    |Varchar	|100				        |			                |Not  Required
29	  	    |		    |Segment21	 	 			                    |Varchar	|100				        |			                |Not  Required
30	  	    |		    |Segment22	 	 			                    |Varchar	|100				        |			                |Not  Required
31	  	    |		    |Segment23	 	 			                    |Varchar	|100				        |			                |Not  Required
32	  	    |		    |Segment24	 	 			                    |Varchar	|100				        |			                |Not  Required
33	  	    |		    |Segment25	 	 			                    |Varchar	|100				        |			                |Not  Required
34	  	    |		    |Segment26	 	 			                    |Varchar	|100				        |			                |Not  Required
35	  	    |		    |Segment27	 	 			                    |Varchar	|100				        |			                |Not  Required
36	  	    |		    |Segment28	 	 			                    |Varchar	|100				        |			                |Not  Required
37	  	    |		    |Segment29	 	 			                    |Varchar	|100				        |			                |Not  Required
38	  	    |		    |Segment30	 	 			                    |Varchar	|100				        |			                |Not  Required
39	  	    |		    |Entered Debit Amount			                |Number		|26 "\\d{1,23}\\.\\d{1,3}" 	|444.00			            |Transaction Debit Amount Please note, that credit and debit amounts cannot both appear on the one journal line
40	  	    |		    |Entered Credit Amount			                |Number		|26 "\\d{1,23}\\.\\d{1,3}" 	|444.00			            |Transaction Credit Amount Please note, that credit and debit amounts cannot both appear on the one journal line
41	  	    |		    |Converted Debit Amount			                |Number		|26 "\\d{1,23}\\.\\d{1,3}" 	|			                |Not  Required
42	  	    |		    |Converted Credit Amount		                |Number		|26 "\\d{1,23}\\.\\d{1,3}" 	|			                |Not  Required
43	  	    |		    |REFERENCE1 (Batch Name)		                |Varchar	|100				        |Multiple jrnl import	    |N/A for Onyx
44	  	    |		    |REFERENCE2 (Batch Description)	                |Varchar	|240				        |			                |N/A for Onyx
45	  	    |		    |REFERENCE3	 	 			                    |Varchar	|100				        |			                |Not Required
46	  	    |		    |REFERENCE4 (Journal Entry Name)    		    |Varchar	|100				        |			                |Not Required
47	  	    |		    |REFERENCE5 (Journal Entry Description)		    |Varchar	|240				        |			                |Not Required
48	  	    |		    |REFERENCE6 (Journal Entry Reference)		    |Varchar	|100				        |			                |Not Required
49	  	    |		    |REFERENCE7 (Journal Entry Reversal flag)	    |Varchar	|1				            |			                |Not Required
50	  	    |		    |REFERENCE8 (Journal Entry Reversal Period)	    |Varchar	|100				        |			                |Not Required
51	  	    |		    |REFERENCE9 (Journal Reversal Method)		    |Varchar	|1				            |			                |Not Required
52	  	    |		    |REFERENCE10 (Journal Entry Line Description)	|Varchar	|240				        |			                |Not Required
53	  	    |		    |Reference column 1	 	 		                |Varchar	|100				        |			                |This field shall store the above value which is required from the ADP feed, it shall be stripped out as part of the ETL process and so shall be seamless to others and NOT get to load into Fusion GL.	Not Required * for HR ADP feed only - to hold "Payroll File ID
54	  	    |		    |Reference column 2				                |Varchar	|100				        |			                |As above	Not Required * for HR ADP feed only - to hold "Literal Description”
55	  	    |		    |Reference column 3				                |Varchar	|100				        |			                |As above	Not Required * for HR ADP feed only - to hold "Client ID ("ADPTP" text constant)", see note:
56	  	    |		    |Reference column 4	 	 	 	                |Varchar	|100				        |			                |As above	Not Required * for HR ADP feed only - to hold "Reversal Date (whenever the trx is an accrual or a reversal)"
57	  	    |		    |Reference column 5				                |Varchar	|100				        |			                |Not Required
58	  	    |		    |Reference column 6				                |Varchar	|100				        |			                |Not Required
59	  	    |		    |Reference column 7	 	 	 	                |Varchar	|100				        |			                |Not Required
60	  	    |		    |Reference column 8				                |Varchar	|100				        |			                |Not Required
61	  	    |		    |Reference column 9				                |Varchar	|100				        |			                |Not Required
62	  	    |		    |Reference column 10				            |Varchar	|100				        |			                |Not Required
63	  	    |		    |Statistical Amount				                |Number		|18				            |			                |N/A for Onyx
64	  	    |		    |(L) Currency Conversion Type			        |Varchar	|30				            |			                |Not Required
65	  	    |		    |Currency Conversion Date			            |Date		|YYYY/MM/DD			        |			                |Not Required
66	  	    |		    |Currency Conversion Rate			            |Number		|18				            |			                |Not Required
67	  	    |		    |Interface Group Identifier			            |Number		|18				            |			                |Start with 1 and increment by 1 for each new file     Not Required
68	  	    |		    |Context field for Journal Entry Line DFF	    |Varchar	|100				        |			                |Not Required
69	  	    |		    |Case Number					                |Varchar	|150				        |Case Number		        |N/A for Onyx
70	  	    |		    |COLI File Date					                |Varchar	|150				        |COLI File Date		        |N/A for Onyx
71	  	    |		    |COLI Applied Date				                |Varchar	|150				        |COLI Applied Date	        |N/A for Onyx
72	  	    |		    |Policy Number					                |Varchar	|150				        |Policy Number		        |Policy Number Policy Number for all transactions. The Trading system will not have any policyholder information so leave blank or feed 00000. For the commission system, the policy number will be applicable on all except for Trail commission. Leave blank if N/A
73	  	    |		    |Treaty Name					                |Varchar	|150				        |Treaty Name		        |N/A for Onyx
74	  	    |		    |Treaty Reference Number			            |Varchar	|150				        |Treaty Reference Number    |N/A for Onyx
75	  	    |		    |Reinsurer Name					                |Varchar	|150				        |Reinsurer Name		        |N/A for Onyx
76	  	    |		    |Plan Code					                    |Varchar	|150				        |Plan Code		            |Plan Code of the Product. Se2 uses 739 for Masters Prime and has different plan code for each product. TBD what our plan code will be for Masters Prime.
77	  	    |		    |Product Name					                |Varchar	|150				        |Product Name		        |“Masters Prime” (this will vary as we add other product to Onyx)
78	  	    |		    |Municipality					                |Varchar	|150				        |Municipality		        |N/A for Onyx
79	  	    |		    |Cash Transaction Type				            |Varchar	|150				        |Cash Transaction Type	    |N/A for Onyx
80	  	    |		    |Check Number					                |Varchar	|150				        |Check Number		        |Check Number on outgoing payment, if applicable
81	  	    |		    |Payment Date					                |Varchar	|150				        |Payment Date		        |Payment Date on Journal Line
82	  	    |		    |Payee Name					                    |Varchar	|150				        |Payee Name		            |Payee Name on Journal Line
83	  	    |		    |EBS COV String					                |Varchar	|150				        |EBS COV String		        |N/A for Onyx
84	  	    |		    |Attribute15 Value for Captured Information DFF	|Varchar	|100				        |			                |Not Required
85	  	    |		    |Attribute16 Value for Captured Information DFF	|Varchar	|100				        |			                |Not Required
86	  	    |		    |Attribute17 Value for Captured Information DFF	|Varchar	|100				        |			                |Not Required
87	  	    |		    |Attribute18 Value for Captured Information DFF	|Varchar	|100				        |			                |Not Required
88	  	    |		    |Attribute19 Value for Captured Information DFF	|Varchar	|100				        |			                |Not Required
89	  	    |		    |Attribute20 Value for Captured Information DFF	|Varchar	|100				        |			                |Not Required
90	  	    |		    |Average Journal Flag	 	 	 	            |Varchar	|100				        |			                |Not Required
91	  	    |		    |Clearing Company				                |Varchar	|100				        |			                |Not Required
92	  	    |Y		    |END						                    |Varchar	|3				            |END			            |To mark the end of each record.


 */

@JsonPropertyOrder(
    "statusCode",
    "ledgerId",
    "effectDateOfTransaction",
    "journalSource",
    "journalCategory",
    "currencyCode",
    "journalEntryCreationDate",
    "actualFlag",
    "legalEntity",
    "majorProduct",
    "account",
    "costCenter",
    "residence",
    "reinsurance",
    "interCompany",
    "businessType",
    "marketSegment",
    "fund",
    "segment01",
    "segment02",
    "segment03",
    "segment04",
    "segment05",
    "segment06",
    "segment07",
    "segment08",
    "segment09",
    "segment10",
    "segment21",
    "segment22",
    "segment23",
    "segment24",
    "segment25",
    "segment26",
    "segment27",
    "segment28",
    "segment29",
    "segment30",
    "enteredDebitAmount",
    "enteredCreditAmount",
    "convertedDebitAmount",
    "convertedCreditAmount",
    "reference01",
    "reference02",
    "reference03",
    "reference04",
    "reference05",
    "reference06",
    "reference07",
    "reference08",
    "reference09",
    "reference10",
    "adpReference01",
    "adpReference02",
    "adpReference03",
    "adpReference04",
    "adpReference05",
    "adpReference06",
    "adpReference07",
    "adpReference08",
    "adpReference09",
    "adpReference10",
    "statisticalAmount",
    "currencyConversionType",
    "currencyConversionDate",
    "currencyConversionRate",
    "interfaceGroupIdentifier",
    "contextFieldForJournalEntryLineDFF",
    "caseNumber",
    "coliFileDate",
    "coliAppliedDate",
    "policyNumber",
    "treatyName",
    "treatyReferenceNumber",
    "reinsurerName",
    "planCode",
    "productName",
    "municipality",
    "cashTransactionType",
    "checkNumber",
    "paymentDate",
    "payeeName",
    "ebsCovString",
    "attribute15ValueOfCapturedInformationDFF",
    "attribute16ValueOfCapturedInformationDFF",
    "attribute17ValueOfCapturedInformationDFF",
    "attribute18ValueOfCapturedInformationDFF",
    "attribute19ValueOfCapturedInformationDFF",
    "attribute20ValueOfCapturedInformationDFF",
    "averageJournalFlag",
    "clearingCompany",
    "endOfRow"
)
data class LedgerDetailRow(
    var statusCode: String = "NEW",
    var ledgerId: Long,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    var effectDateOfTransaction: LocalDate,
    var journalSource: String,
    var journalCategory: String,
    var currencyCode: String = "USD",
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    var journalEntryCreationDate: LocalDate,
    var actualFlag: String = "A",
    var legalEntity: String,
    var majorProduct: String,
    var account: String,
    var costCenter: String,
    var residence: String,
    var reinsurance: String,
    var interCompany: String,
    var businessType: String,
    var marketSegment: String,
    var fund: String,
    var segment01: String? = null,
    var segment02: String? = null,
    var segment03: String? = null,
    var segment04: String? = null,
    var segment05: String? = null,
    var segment06: String? = null,
    var segment07: String? = null,
    var segment08: String? = null,
    var segment09: String? = null,
    var segment10: String? = null,
    var segment21: String? = null,
    var segment22: String? = null,
    var segment23: String? = null,
    var segment24: String? = null,
    var segment25: String? = null,
    var segment26: String? = null,
    var segment27: String? = null,
    var segment28: String? = null,
    var segment29: String? = null,
    var segment30: String? = null,
    @JsonRawValue
    var enteredDebitAmount: BigDecimal? = null,
    @JsonRawValue
    var enteredCreditAmount: BigDecimal? = null,
    @JsonRawValue
    var convertedDebitAmount: BigDecimal? = null,
    @JsonRawValue
    var convertedCreditAmount: BigDecimal? = null,
    var reference01: String? = null,
    var reference02: String? = null,
    var reference03: String? = null,
    var reference04: String? = null,
    var reference05: String? = null,
    var reference06: String? = null,
    var reference07: String? = null,
    var reference08: String? = null,
    var reference09: String? = null,
    var reference10: String? = null,
    var adpReference01: String? = null,
    var adpReference02: String? = null,
    var adpReference03: String? = null,
    var adpReference04: String? = null,
    var adpReference05: String? = null,
    var adpReference06: String? = null,
    var adpReference07: String? = null,
    var adpReference08: String? = null,
    var adpReference09: String? = null,
    var adpReference10: String? = null,
    var statisticalAmount: BigDecimal? = null,
    var currencyConversionType: String? = null,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    var currencyConversionDate: LocalDate? = null,
    var currencyConversionRate: Long? = null,
    var interfaceGroupIdentifier: Long? = null,
    var contextFieldForJournalEntryLineDFF: String? = null,
    var caseNumber: String? = null,
    var coliFileDate: String? = null,
    var coliAppliedDate: String? = null,
    var policyNumber: String? = null,
    var treatyName: String? = null,
    var treatyReferenceNumber: String? = null,
    var reinsurerName: String? = null,
    var planCode: String? = null,
    var productName: String? = null,
    var municipality: String? = null,
    var cashTransactionType: String? = null,
    var checkNumber: String? = null,
    var paymentDate: String? = null,
    var payeeName: String? = null,
    var ebsCovString: String? = null,
    var attribute15ValueOfCapturedInformationDFF: String? = null,
    var attribute16ValueOfCapturedInformationDFF: String? = null,
    var attribute17ValueOfCapturedInformationDFF: String? = null,
    var attribute18ValueOfCapturedInformationDFF: String? = null,
    var attribute19ValueOfCapturedInformationDFF: String? = null,
    var attribute20ValueOfCapturedInformationDFF: String? = null,
    var averageJournalFlag: String? = null,
    var clearingCompany: String? = null,
    var endOfRow: String = "END"
)

/**
 *
 *
# Ledger Control Record Format

Position    |Required	| Name        					                |Type    	|Size/Format  			    |Example    		        |Comment
------------|:---------:|:----------------------------------------------|-----------|--------------------------:|:-------------------------:|:---------------------------------------------------------------
1	  	    |Y		    |Status Code			                        |Varchar	|3		                    |CTL            			|Should be set to CTL to indicate it’s a control record for the file
2	  	    |Y 		    |Ledger ID			                            |Number		|18		                    |201		            	|The total number of records in this file
3	  	    |Y		    |Effective Date of Transaction	                |Date		|YYYY/MM/DD	                |2019/05/04		            |Date of the file (YYYY/MM/DD)
4	  	    |Y		    |(L)Journal Source		                        |Varchar	|25		                    |20000			            |The total sum of all the credits in the file
5	  	    |Y		    |(L)Journal Category		                    |Varchar	|25		                    |20000			            |The total sum of all the Debits  in the file
6-91	  	|		    |Nothing			                            |Varchar	|100		                |	    		            |empty for remaining columns
92	  	    |Y		    |END				                            |Varchar	|3		                    |END			            |To mark the end of each record.


 This data class uses the LedgerDetailRow as the schema, if you want to create a schema with this pojo,
 then you would need to add a field to prepresent the nothing columns from 6-91.  By using the detail row scheema,
 it maps the missing fields since they are all optional.
 */


data class LedgerControlRow(
    var statusCode: String = "CTL",
    var ledgerId: Long,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DATE_FORMAT)
    var effectDateOfTransaction: LocalDate,
    @JsonRawValue
    var journalSource: BigDecimal,
    @JsonRawValue
    var journalCategory: BigDecimal,
    var endOfRow: String = "END"
)