package com.delawarelife.example.file.record

import com.fasterxml.jackson.databind.ObjectWriter
import java.io.OutputStream

class CSVRecordStream(private val writer : ObjectWriter, private val outStream : OutputStream) : RecordStream() {

    override fun addFooter(footerRecord: Any): CSVRecordStream {
        writer.writeValue(outStream,footerRecord)
        return this;
    }

    override fun addRecord(dataRecord: Any): CSVRecordStream {
        writer.writeValue(outStream, dataRecord)
        return this;
    }

    override fun close() {
        outStream.flush()
        outStream.close()
    }

}
