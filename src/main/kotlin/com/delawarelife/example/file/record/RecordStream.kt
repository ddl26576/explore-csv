package com.delawarelife.example.file.record

import java.io.Closeable

abstract class RecordStream : Closeable {
        abstract fun addFooter(endRecord: Any): RecordStream
        abstract fun addRecord(detailRecord: Any): CSVRecordStream
}