package com.delawarelife.example.file.record

import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.time.LocalDate
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals

class CSVRecordStreamBuilderTest {
    @Test
    fun testRecordStreamExport() {
        val outputStream = ByteArrayOutputStream()
        val csvRecordStream = CSVRecordStreamBuilder()
            .spec(LedgerDetailRow::class)
            .build(outputStream)

        csvRecordStream.use {
            val row1 = createLedgerDetailRow()
            csvRecordStream.addRecord(row1)

            val endRow = createLedgerControlRow()
            csvRecordStream.addFooter(endRow);
        }
        val output = String(outputStream.toByteArray())
        assertNotEquals(0, output.length, "stream returned")

        val expected = """
            NEW,1234567890,1990/01/01,JOURNAL_SOURCE,JOURNAL_CATEGORY,USD,2020/01/14,A,"Legal Entity, LLC","Major Product Id",Account_12345,COST_CENTER,RESIDENCE,REINSURANCE,INTER_COMPANY,BUSINESS_TYPE,MARKET_SEGMENT,FUND,,,,,,,,,,,,,,,,,,,,,12345678901234567890123.45,,0.00,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,END
            CTL,1234567890,1990/01/01,1234567890123456789012.34,1234567890123456789012.34,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,END
        """.trimIndent().plus("\n")
        assertEquals(expected, output)
    }

    @Test
    fun testRecordStreamExport2() {
        val outputStream = ByteArrayOutputStream()
        val csvRecordStream = CSVRecordStreamBuilder()
            .spec(LedgerDetailRow::class)
            .build(outputStream)

        csvRecordStream.use { csv ->
            val row1 = createLedgerDetailRow()
            val row2 = createLedgerDetailRow().apply {
                ledgerId = 1234567891
            }
            csv.addRecord(row1)
            csv.addRecord(row2)

            val endRow = createLedgerControlRow()
            csv.addFooter(endRow)
        }

        val output = String(outputStream.toByteArray())
        assertNotEquals(0, output.length, "stream returned")

        val expected = """
            NEW,1234567890,1990/01/01,JOURNAL_SOURCE,JOURNAL_CATEGORY,USD,2020/01/14,A,"Legal Entity, LLC","Major Product Id",Account_12345,COST_CENTER,RESIDENCE,REINSURANCE,INTER_COMPANY,BUSINESS_TYPE,MARKET_SEGMENT,FUND,,,,,,,,,,,,,,,,,,,,,12345678901234567890123.45,,0.00,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,END
            NEW,1234567891,1990/01/01,JOURNAL_SOURCE,JOURNAL_CATEGORY,USD,2020/01/14,A,"Legal Entity, LLC","Major Product Id",Account_12345,COST_CENTER,RESIDENCE,REINSURANCE,INTER_COMPANY,BUSINESS_TYPE,MARKET_SEGMENT,FUND,,,,,,,,,,,,,,,,,,,,,12345678901234567890123.45,,0.00,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,END
            CTL,1234567890,1990/01/01,1234567890123456789012.34,1234567890123456789012.34,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,END
        """.trimIndent().plus("\n")
        assertEquals(expected, output)
    }

    @Test
    fun testRecordStreamExportWithHeader() {
        val outputStream = ByteArrayOutputStream()
        val csvRecordStream = CSVRecordStreamBuilder()
            .spec(LedgerDetailRow::class)
            .header()
            .build(outputStream)

        csvRecordStream.use {
            val row1 = createLedgerDetailRow()
            csvRecordStream.addRecord(row1)

            val endRow = createLedgerControlRow()
            csvRecordStream.addFooter(endRow);
        }
        val output = String(outputStream.toByteArray())
        assertNotEquals(0, output.length, "stream returned")

        val expected = """
            statusCode,ledgerId,effectDateOfTransaction,journalSource,journalCategory,currencyCode,journalEntryCreationDate,actualFlag,legalEntity,majorProduct,account,costCenter,residence,reinsurance,interCompany,businessType,marketSegment,fund,segment01,segment02,segment03,segment04,segment05,segment06,segment07,segment08,segment09,segment10,segment21,segment22,segment23,segment24,segment25,segment26,segment27,segment28,segment29,segment30,enteredDebitAmount,enteredCreditAmount,convertedDebitAmount,convertedCreditAmount,reference01,reference02,reference03,reference04,reference05,reference06,reference07,reference08,reference09,reference10,adpReference01,adpReference02,adpReference03,adpReference04,adpReference05,adpReference06,adpReference07,adpReference08,adpReference09,adpReference10,statisticalAmount,currencyConversionType,currencyConversionDate,currencyConversionRate,interfaceGroupIdentifier,"contextFieldForJournalEntryLineDFF",caseNumber,coliFileDate,coliAppliedDate,policyNumber,treatyName,treatyReferenceNumber,reinsurerName,planCode,productName,municipality,cashTransactionType,checkNumber,paymentDate,payeeName,ebsCovString,"attribute15ValueOfCapturedInformationDFF","attribute16ValueOfCapturedInformationDFF","attribute17ValueOfCapturedInformationDFF","attribute18ValueOfCapturedInformationDFF","attribute19ValueOfCapturedInformationDFF","attribute20ValueOfCapturedInformationDFF",averageJournalFlag,clearingCompany,endOfRow
            NEW,1234567890,1990/01/01,JOURNAL_SOURCE,JOURNAL_CATEGORY,USD,2020/01/14,A,"Legal Entity, LLC","Major Product Id",Account_12345,COST_CENTER,RESIDENCE,REINSURANCE,INTER_COMPANY,BUSINESS_TYPE,MARKET_SEGMENT,FUND,,,,,,,,,,,,,,,,,,,,,12345678901234567890123.45,,0.00,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,END
            CTL,1234567890,1990/01/01,1234567890123456789012.34,1234567890123456789012.34,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,END
        """.trimIndent().plus("\n")
        assertEquals(expected, output)
    }

    @Test
    fun testRecordStreamExportSpecMissingWithBuild() {
        assertFailsWith<IllegalArgumentException>("expected failure due to missing spec") {
            val outputStream = ByteArrayOutputStream()
            CSVRecordStreamBuilder()
                .build(outputStream)
        }
    }

    private fun createLedgerDetailRow(): LedgerDetailRow {
        val row = LedgerDetailRow(
            ledgerId = 1234567890L,
            effectDateOfTransaction = LocalDate.of(1990, 1, 1),
            journalSource = "JOURNAL_SOURCE",
            journalCategory = "JOURNAL_CATEGORY",
            currencyCode = "USD",
            journalEntryCreationDate = LocalDate.now(),
            actualFlag = "A",
            legalEntity = "Legal Entity, LLC",
            majorProduct = "Major Product Id",
            account = "Account_12345",
            costCenter = "COST_CENTER",
            residence = "RESIDENCE",
            reinsurance = "REINSURANCE",
            interCompany = "INTER_COMPANY",
            businessType = "BUSINESS_TYPE",
            marketSegment = "MARKET_SEGMENT",
            enteredDebitAmount = "12345678901234567890123.45".toBigDecimal().setScale(2),
            convertedDebitAmount = "0".toBigDecimal().setScale(2),
            fund = "FUND"
        )
        return row
    }

    private fun createLedgerControlRow(): LedgerControlRow {
        val endRow = LedgerControlRow(
            ledgerId = 1234567890L,
            effectDateOfTransaction = LocalDate.of(1990, 1, 1),
            journalSource = "1234567890123456789012.34".toBigDecimal().setScale(2),
            journalCategory = "1234567890123456789012.34".toBigDecimal().setScale(2)
        )
        return endRow
    }
}