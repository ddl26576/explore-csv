package com.delawarelife.example.file.record

import com.fasterxml.jackson.dataformat.csv.CsvMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.junit.jupiter.api.Test
import java.time.LocalDate
import kotlin.test.assertNotNull


class CSVRecordStreamExportTest {

    @Test
    fun testCVSSimpleOutput() {
//        val writer = ByteArrayOutputStream()
//        val csvSchema = CsvSchema.builder()
//            .addColumn("firstName")
//            .addColumn("lastName")
//            .addColumn("age", CsvSchema.ColumnType.NUMBER)
//            .build()
        val mapper = CsvMapper()
        mapper.registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
        val detailRecord = LedgerDetailRow::class.java

        val csvSchema = mapper.schemaFor(detailRecord)

        csvSchema.withoutQuoteChar()
            .withColumnSeparator(',')
            .withLineSeparator("\n")
            .withUseHeader(true)
            .withAllowComments(true)

        val row1 = createLedgerDetailRow()

        val writer = mapper.writer(csvSchema)

        val csv: String = writer.writeValueAsString(row1)
        println("csv=$csv")
        assertNotNull(csv, "cvs returned")

        val endRow = createLedgerControlRow()

        val csvEnd: String = writer.writeValueAsString(endRow)
        println("csvEnd=$csvEnd")
        assertNotNull(csvEnd, "cvsEnd returned")
    }

    @Test
    fun testRecordStreamExport() {
        val mapper = CsvMapper()
        mapper.registerModule(KotlinModule())
            .registerModule(JavaTimeModule())
        val detailRecord = LedgerDetailRow::class.java

        val csvSchema = mapper.schemaFor(detailRecord)

        csvSchema.withoutQuoteChar()
            .withColumnSeparator(',')
            .withLineSeparator("\n")
            .withUseHeader(true)
            .withAllowComments(true)

        val row1 = createLedgerDetailRow()

        val writer = mapper.writer(csvSchema)

        val csv: String = writer.writeValueAsString(row1)
        println("csv=$csv")
        assertNotNull(csv, "cvs returned")

        val endRow = createLedgerControlRow()

        val csvEnd: String = writer.writeValueAsString(endRow)
        println("csvEnd=$csvEnd")
        assertNotNull(csvEnd, "cvsEnd returned")
    }

    private fun createLedgerDetailRow(): LedgerDetailRow {
        val row = LedgerDetailRow(
            ledgerId = 1234567890L,
            effectDateOfTransaction = LocalDate.of(1990, 1, 1),
            journalSource = "JOURNAL_SOURCE",
            journalCategory = "JOURNAL_CATEGORY",
            currencyCode = "USD",
            journalEntryCreationDate = LocalDate.now(),
            actualFlag = "A",
            legalEntity = "Legal Entity, LLC",
            majorProduct = "Major Product Id",
            account = "Account_12345",
            costCenter = "COST_CENTER",
            residence = "RESIDENCE",
            reinsurance = "REINSURANCE",
            interCompany = "INTER_COMPANY",
            businessType = "BUSINESS_TYPE",
            marketSegment = "MARKET_SEGMENT",
            enteredDebitAmount = "12345678901234567890123.456".toBigDecimal().setScale(3),
            convertedDebitAmount = "0".toBigDecimal().setScale(3),
            fund = "FUND"
        )
        return row
    }

    private fun createLedgerControlRow(): LedgerControlRow {
        val endRow = LedgerControlRow(
            ledgerId = 1234567890L,
            effectDateOfTransaction = LocalDate.of(1990, 1, 1),
            journalSource = "1234567890123456789012.34".toBigDecimal().setScale(2),
            journalCategory = "1234567890123456789012.34".toBigDecimal().setScale(2)
        )
        return endRow
    }
}