package com.delawarelife.example.file

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import kotlin.test.assertNotNull

@SpringBootTest
class FileGeneratorApplicationTests {

    @Test
    fun contextLoads() {
    }

    @Test
    fun contextSeen(ctx : ApplicationContext) {
        assertNotNull(ctx,"application context")
    }

}
